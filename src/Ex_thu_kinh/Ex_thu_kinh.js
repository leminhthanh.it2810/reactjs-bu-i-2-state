import React, { Component } from "react";
import { dataGlasses } from "./data_glasses.js";
export default class Ex_thu_kinh extends Component {
  state = {
    selectedGlasses: null,
  };

  handleSelectGlasses = (glasses) => {
    this.setState({ selectedGlasses: glasses });
  };
  render() {
    const { selectedGlasses } = this.state;
    return (
      <div style={{
        backgroundImage: "url(./glassesImage/background.jpg)",
        backgroundSize: "cover",
        height: "100vh",
      }}>
        <div className="container">
          <div className="position-relative">
            {selectedGlasses ? (
              <img
                className="position-absolute"
                style={{ width: "150px", top: "75px", left: "480px" }}
                src={selectedGlasses.url}
                alt=""
              />
            ): null}
            <img
              className=""
              style={{ width: "250px" }}
              src="./glassesImage/model.jpg"
              alt="Model"
            />
            <div>
              {dataGlasses.map((glasses) => (
                <button onClick={() => this.handleSelectGlasses(glasses)}>
                  <img style={{ width: "150px" }} src={glasses.url} alt="" />
                </button>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
